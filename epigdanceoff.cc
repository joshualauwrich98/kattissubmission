#include<string>
#include<iostream>

using namespace std;

int main() {
    int n,m,i,j;
    string temp;
    cin >> n >> m;
    string data[n];
    getline(cin, temp);
    for (i = 0; i < n; i++) {
        getline(cin, data[i]);
    }

    int total = 0;
    bool status = true;
    for (i = 0; i < m; i++) {
        status = true;
        for (j = 0; j < n; j++) {
            if (data[j][i] != '_') {
                status = false;
                break;
            }
        }
        if (status) total++;
    }

    total++;

    printf("%d\n", total);

    return 0;
}