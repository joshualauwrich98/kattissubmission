#include <stdio.h>
#include <algorithm>

using namespace std;

int main () {
	int kasus, byk, i, j;
	scanf("%d", &kasus);
	for (j = 0; j < kasus; j++) {
		scanf("%d", &byk);
		int data[byk];
		for (i = 0; i < byk; i++) {
			scanf("%d", &data[i]);
		}
		sort(data, data+byk);
		
		int res = -1;
		for (i = 0; i < byk-1; i+=2) {
			if  (data[i] != data[i+1]) {
				res = data[i];
				break;
			}
		}
		
		if (res == -1) res = data[byk-1];
		printf("Case #%d: %d\n", j+1, res);
	}
	return 0;
}
