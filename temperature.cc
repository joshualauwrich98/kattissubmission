#include<stdio.h>

int main() {
    double a,b;
    scanf("%lf %lf", &a, &b);
    if (a == 0 && b == 1) printf("ALL GOOD\n");
    else if (b == 1) printf("IMPOSSIBLE\n");
    else {
        double temp = a/(b-1);
        double res = a - b*temp;
        printf("%.7lf\n", res);
    }
    

    return 0;
}