#include <iostream>
#include <string>
#include <set>

using namespace std;

int main () {
	set<string> st;
	string temp;
	int kasus, n;
	scanf("%d", &kasus);
	while (kasus--) {
		scanf("%d", &n);
		st.clear();
		for(int i = 0; i < n; i++) {
			cin >> temp;
			st.insert(temp);
		}
		
		printf("%d\n", st.size());
	}
	
	return 0;
}
