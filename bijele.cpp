#include <stdio.h>

int king = 1;
int queen = 1;
int rooks = 2;
int bishops = 2;
int knights = 2;
int pawns = 8;

int main () {
	int k,q,r,b,kn,p;
	scanf("%d %d %d %d %d %d", &k, &q, &r, &b, &kn, &p);
	printf("%d %d %d %d %d %d\n", (king-k), (queen-q), (rooks-r), (bishops-b), (knights-kn), (pawns-p));
	return 0;
}
