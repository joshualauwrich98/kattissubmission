#include<string>
#include<iostream>
#include<utility>
#include <iomanip>

using namespace std;

int main() {
    int kasus, i, j, r, p, d;
    double scaled_weight, temp, w, percent;
    string input;
    cout << fixed; 
    cout << setprecision(1);

    cin >> kasus;
    for(i = 1; i <= kasus; i++) {
        cout << "Recipe # " << i << endl;
        cin >> r >> p >> d;
        temp = (d*1.0)/(p*1.0);
        pair<string, pair < double, double > > data[r];
        for(j = 0; j < r; j++) {
            cin >> input >> w >> percent;

            if (percent == 100.0) {
                scaled_weight = w*temp;
            }

            data[j] = make_pair(input, make_pair(w, percent));
        }
        
        for (j = 0; j < r; j++) {
            temp = data[j].second.second/100.0 * scaled_weight;
            cout << data[j].first << " " << temp << endl;
        }
        cout << "----------------------------------------" << endl;
    }

    return 0;
}