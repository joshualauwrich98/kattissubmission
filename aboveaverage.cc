#include<stdio.h>
#include <math.h>

int main() {
    int kasus, n, i, temp;
    double sum, res;
    scanf("%d", &kasus);
    while(kasus--) {
        scanf("%d", &n);
        sum = 0;
        res = 0;
        int data[n];
        for (i = 0; i < n; i++) {
            scanf("%d", &data[i]);
            sum += data[i];
        }

        sum = sum / (n*1.0);

        temp = 0;
        for(i = 0; i < n; i++) {
            if (data[i] > sum) {
                temp++;
            }
        }

        res = (temp*1.0)/(n*1.0)*100.0;
        printf("%.3f%%\n", roundf(res * 1000) / 1000);
    }
    return 0;
}