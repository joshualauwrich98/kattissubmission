#include<stdio.h>
#include<algorithm>

using namespace std;

int main() {
    int n, i, j, temp = 0, total = 0;
    scanf("%d", &n);
    int data[n];
    for(i = 0; i < n; i++) {
        scanf("%d", &data[i]);
        total += data[i];
    }

    sort(data, data+n);

    for(i = n-3; i >= 0; i-=3) {
        temp += data[i];
    }

    printf("%d\n", total-temp);

    return 0;
}