#include<string>
#include<iostream>
#include<ctype.h>

using namespace std;

bool isAlphabet(char c) {
    return c >= 'a' && c <= 'z';
}

int main() {
    string dictionary[26] = {
        "@", "8", "(", "|)", "3", "#", "6", "[-]", "|", "_|",
        "|<", "1", "[]\\/[]", "[]\\[]", "0", "|D", "(,)",
        "|Z", "$", "']['", "|_|", "\\/", "\\/\\/", "}{",
        "`/", "2"
    };

    string input, res ="";
    int i, j;
    getline(cin, input);
    for (i = 0; i < input.length(); i++) {
        char c = (char) (tolower(input[i]));
        if (isalpha(c)) {
            res += dictionary[c-'a'];
        } else {
            res += input[i];
        }
    }
    cout << res << endl;
    return 0;
}