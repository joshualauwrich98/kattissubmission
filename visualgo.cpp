

// C++ program to print all paths from a source to destination.
#include<iostream>
#include <stdio.h>
#include <list>
#include <utility>
using namespace std;
int c=0,t=0;
// A directed graph using adjacency list representation
class Graph
{
    int V;    // No. of vertices in graph
    list<pair<int,int> > *adj; // Pointer to an array containing adjacency lists
 	
    void printAllPathsUtil(const pair<int , int> &u, int d, bool visited[], int path[], int &index);
 
public:
    Graph(int V);   // Constructor
    void addEdge(int u, int v, int w);
    void printAllPaths(int s, int d);
};
 
Graph::Graph(int V)
{
    this->V = V;
    adj = new list<pair<int,int> >[V];
}
 
void Graph::addEdge(int u, int v, int w)
{
    adj[u].push_back(make_pair(v,w)); // Add v to u�s list.
}
 
// Prints all paths from 's' to 'd'
void Graph::printAllPaths(int s, int d)
{
    bool *visited = new bool[V];
 
    int *path = new int[V];
    int path_index = 0; // Initialize path[] as empty
 
    for (int i = 0; i < V; i++)
        visited[i] = false;
 	c=0;t=0;
    printAllPathsUtil(&adj[s], d, visited, path, path_index);
}
void Graph::printAllPathsUtil(const pair <int,int> &u, int d, bool visited[],
                              int path[], int &path_index)
{
    visited[u] = true;
    path[path_index] = u;
    path_index++;
 
    if (u == d)
    {
    	int temp = 0 ;
        for (int i = 0; i<path_index; i++)
            cout << path[i].first << " ";
        cout << endl;
    }
    else // If current vertex is not destination
    {
        list<int>::iterator i;
        for (i = adj[u].begin(); i != adj[u].end(); ++i)
            if (!visited[*i.first])
                printAllPathsUtil(i, d, visited, path, path_index);
    }
 
    path_index--;
    visited[u] = false;
}
 
// Driver program
int main()
{
    // Create a graph given in the above diagram
    Graph g(4);
    g.addEdge(0, 1,2);
    g.addEdge(0, 2,3);
    g.addEdge(0, 3,4);
    g.addEdge(2, 0,3);
    g.addEdge(2, 1,2);
    g.addEdge(1, 3,1);
 
    int s = 2, d = 3;
    cout << "Following are all different paths from " << s
         << " to " << d << endl;
 	g.printAllPaths(s,d);
    return 0;
}
