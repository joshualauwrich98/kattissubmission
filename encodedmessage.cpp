#include <stdio.h>
#include <math.h>
#include <string.h>

#define MAX 10005

int main () {
	int kasus, length, i, n, j, k;
	char kata[MAX];
	scanf("%d", &kasus);
	fgets(kata, MAX, stdin);
	while (kasus--) {
		fgets(kata, MAX, stdin);
		length = strlen(kata);
		n = sqrt(length);
		char arr[n][n];
		k = 0;
		for(i = 0; i < n; i++) {
			for (j = 0; j < n; j++) {
				arr[i][j] = kata[k++];
			}
		}
		
		for (i = n-1; i >= 0; i--) {
			for (j = 0; j < n; j++) {
				printf("%c", arr[j][i]);
			}
		}
		printf("\n");
	}
	return 0;
}
