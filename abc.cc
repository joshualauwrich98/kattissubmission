#include<stdio.h>
#include<string.h>
#include<algorithm>

using namespace std;

int main() {
    int num1, num2, num3;
    char data[4];
    scanf("%d %d %d", &num1, &num2, &num3);
    int temp[3] = {num1, num2, num3};
    scanf("%s", data);
    sort(temp, temp+3);

    for(int i = 0; i < 3; i++) {
        if (data[i] == 'C') {
            if (i == 0) printf("%d", temp[2]);
            else printf(" %d", temp[2]);
        } else if(data[i] == 'B') {
            if (i == 0) printf("%d", temp[1]);
            else printf(" %d", temp[1]);
        } else {
            if (i == 0) printf("%d", temp[0]);
            else printf(" %d", temp[0]);
        }
    }
    printf("\n");

    return 0;
}