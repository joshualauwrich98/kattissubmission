#include<map>
#include<string>
#include<utility>
#include<iostream>
#include<vector>
#include <algorithm>

using namespace std;

vector<string> split(string s, string delimiter)
{
   vector<string> result;
   size_t pos = 0;
   string token;
   while ((pos = s.find(delimiter)) != string::npos) {
      token = s.substr(0, pos);
      result.push_back(token);
      s.erase(0, pos + delimiter.length());
   }
   result.push_back(s);
   return result;
}

int main() {
   string input, res;
   int i, temp, total;
   vector<string> temp1;
   map<string, int> data;
   bool found;
   while(getline(cin, input)) {
      temp1 = split(input, " ");
      if (temp1[0].compare("clear") == 0) data.clear();
      else if(temp1[0].compare("def") == 0) {
         auto temp3 = data.insert({temp1[1], atoi(temp1[2].c_str())});
         if (!temp3.second) {
            data.find(temp1[1])->second = atoi(temp1[2].c_str());
         }
      } else {
         found = true;
         total = 0;
         res = "";
         res += temp1[1] + " ";
         if (data.find(temp1[1]) == data.end()) {
            found = false;
         } else {
            total = data.find(temp1[1])->second;
         }
         for (i = 3; i < temp1.size()-1; i+=2) {
            res += temp1[i-1] + " " + temp1[i] + " ";
            if (!found) continue;
            else {
               if (data.find(temp1[i]) == data.end()) {
                  found = false;
                  continue;
               } else {
                  if (temp1[i-1][0] == '+') {
                     total += data.find(temp1[i])->second;
                  } else {
                     total -= data.find(temp1[i])->second;
                  }
               }
            }
         }
         if (found) {
            auto result = find_if(
            data.begin(),
            data.end(),
            [total](const auto& mo) {return mo.second == total; });

            if (result != data.end()) {
               res += "= " + result->first;
            } else {
               res += "= unknown";
            }
            cout << res << endl;
         } else {
            cout << res << "= unknown\n";
         }
      }
   }

    return 0;
}