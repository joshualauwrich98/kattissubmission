#include <stdio.h>

int main () {
	int n, temp, sum = 0;
	scanf("%d", &n);
	int max = n;
	for (int i = 0; i < max; i++) {
		scanf("%d", &temp);
		if (temp == -1) {
			n--;
		} else {
			sum += temp;
		}
	}
	
	printf("%llf\n", ((sum*1.0)/(n*1.0)));
	return 0;
}
