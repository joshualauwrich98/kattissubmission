#include<stdio.h>
#include<math.h>
#include<iostream>
#include<iomanip>

using namespace std;

int main() {
    double input;
    scanf("%lf", &input);
    double res = sqrt(input)*4.0;
    cout << setprecision(20);
    cout << res << endl;

    return 0;
}