#include <stdio.h>

int reverseNumber(int num) {
	int res = 0;
	while (num > 0) {
		res *= 10;
		res += num%10;
		num /= 10;
	}
	return res;
}

int main () {
	int num1, num2;
	scanf("%d %d", &num1, &num2);
	num1 = reverseNumber(num1);
	num2 = reverseNumber(num2);
	if (num1 > num2) {
		printf("%d\n", num1);
	} else {
		printf("%d\n", num2);
	}
	return 0;
}
