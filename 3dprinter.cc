#include<stdio.h>

int main() {
    int statues, printer = 1, days = 0;
    scanf("%d", &statues);
    while(printer < statues) {
        printer *= 2;
        days++;
    }
    days++;
    printf("%d\n", days);
    return 0;
}