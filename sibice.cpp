#include <stdio.h>
#include <math.h>

int main () {
	int n, h, w, temp;
	double h1, w1, temp1, m;
	scanf("%d %d %d", &n, &h, &w);
	h1 = h * 1.0;
	w1 = w * 1.0;
	m = sqrt((h1*h1)+(w1*w1));
	for (int i = 0; i < n; i++) {
		scanf("%d", &temp);
		temp1 = temp * 1.0;
		if (temp1 <= h1 || temp1 <= w1 || temp1 <= m) {
			printf("DA\n");
		} else {
			printf("NE\n");
		}
	}
	return 0;
}
