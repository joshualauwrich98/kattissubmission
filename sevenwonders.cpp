#include <stdio.h>
#include <string.h>

int main () {
	char kata[51];
	scanf("%s", kata);
	bool status[3];
	status[0] = false;
	status[1] = false;
	status[2] = false;
	int num[3];
	num[0] = 0;
	num[1] = 0;
	num[2] = 0;
	int min = 100;
	//T=0 c=1 g=2
	for (int i = 0; i < strlen(kata); i++) {
		if (kata[i] == 'T') {
			num[0]++;
			status[0] = true;
		} else if (kata[i] == 'C') {
			num[1]++;
			status[1] = true;
		} else {
			num[2]++;
			status[2] = true;
		}
	}
		
	int res = (num[0] * num[0]) + (num[1] * num[1]) + (num[2] * num[2]);
	if (num[0] < min) min = num[0];
	if (num[1] < min) min = num[1];
	if (num[2] < min) min = num[2];
	
	res += (min*7);
	
	printf("%d\n", res);
	
	return 0;
}
