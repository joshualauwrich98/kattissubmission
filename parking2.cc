#include<stdio.h>

int smin(int a, int b) {
    return a < b ? a : b;
}

int smax(int a, int b) {
    return a > b ? a : b;
}

int main() {
    int n, nr, min, max, val;
    scanf("%d", &n);
    while (n--) {
        scanf("%d", &nr);
        min = 100, max = 0;
        while(nr--) {
            scanf("%d", &val);
            min = smin(min, val);
            max = smax(max, val);
        }
        printf("%d\n", (max-min)*2);
    }
    return 0;
}