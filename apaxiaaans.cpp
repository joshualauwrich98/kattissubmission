#include <stdio.h>
#include <string.h>

int main () {
	char temp, last = '\0';
	while (scanf("%c", &temp) != EOF) {
		if (temp != last) {
			printf("%c", temp);
			last = temp;
		}
	}
	return 0;
}
