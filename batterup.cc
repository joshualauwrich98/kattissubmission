#include<iostream>
#include<iomanip>

using namespace std;

int main() {
    int n, temp, sum = 0, length = 0;

    cin >> n;
    for (int i = 0; i < n; i++) {
        cin >> temp;
        if (temp >= 0) {
            sum += temp;
            length++;
        }
    }

    cout << setprecision(15);

    cout << (sum*1.0)/(length*1.0) << endl;

    return 0;
}