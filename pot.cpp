#include <stdio.h>
#include <math.h>

int main () {
	int res, n, temp, pangkat;
	scanf("%d", &n);
	res = 0;
	for (int i = 0; i < n; i++) {
		scanf("%d", &temp);
		pangkat = temp%10;
		temp/=10;
		res += pow(temp,pangkat);
	}
	
	printf("%d\n", res);
	return 0;
}
