#include<iostream>
#include<algorithm>

using namespace std;

int main() {
    int n,i,total=1;
    long long int max, temp;
    cin >> n >> max;
    long long int data[n];
    for(i = 0; i < n; i++) {
        cin >> data[i];
    }
    sort(data, data+n);
    for(i = 1; i < n; i++) {
        if (data[i]+data[i-1] > max) break;
        else total++;
    }

    cout << total << endl;
    
    return 0;
}