#include<string>
#include<iostream>

using namespace std;

int main() {
    string cipher, key, res;
    getline(cin, cipher);
    getline(cin, key);
    res = "";
    for (int i = 0; i < cipher.length(); i++) {
        int temp = (cipher[i]-'A')-(key[i]-'A');
        if (temp < 0) temp += 26;
        char c = (char) (temp +'A');
        res += c;
        key += c;
        // cout << key << endl;
    }

    cout << res << endl;
    return 0;
}