#include <stdio.h>

int sumDigit(int num) {
	int res = 0;
	while (num > 0) {
		res += num%10;
		num /= 10;
	}
	
	return res;
}

int main () {
	int a,b,x,i,j,n=0,m=0;
	scanf("%d\n%d\n%d", &a,&b,&x);
	for (i = a; i<=b;i++) {
		int temp = sumDigit(i);
		if (temp==x) {
			n = i;
			break;	
		}
	}
	for (j = b; j >= i;j--) {
		int temp = sumDigit(j);
		if (temp==x) {
			m = j;
			break;
		}
	}
	
	printf("%d\n%d", n, m);
	return 0;
}
