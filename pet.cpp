#include <stdio.h>

int main () {
	int data[5][4];
	for (int i = 0; i < 5; i++) {
		for (int j = 0; j < 4; j++) {
			scanf("%d", &data[i][j]);
		}
	}
	int contestant = 1, point = 0, temp;
	for (int i = 0; i < 5; i++) {
		temp = 0;
		for (int j = 0; j < 4; j++) {
			temp += data[i][j];
		}
		if (temp > point) {
			contestant = i+1;
			point = temp;
		}
	}
	
	printf("%d %d\n", contestant, point);
	return 0;
}
