#include <stdio.h>

int main () {
	int h, m;
	scanf("%d %d", &h, &m);
	if (m < 45) {
		m+=60;
		h--;
	}
	m-=45;
	if (h-1 < 0) h+=24;
	printf("%d %d\n", h,m);
	return 0;
}
