#include <stdio.h>

int main () {
	int data[4][4];
	int cmd, i, j, k;
	for (i = 0; i < 4; i++) {
		for (j = 0; j < 4; j++) {
			scanf("%d", &data[i][j]);
		}
	}
	
	scanf("%d", &cmd);
	
	switch(cmd) {
		case 0:
			for (i = 0; i < 4; i++) {
				j = 0;
				for (k = 1; k < 4; k++) {
					if (data[i][j] != 0) {
						if (data[i][j] == data[i][k]) {
							data[i][j] += data[i][k];
							data[i][k] = 0;
							j++;
						} else if (data[i][k]==0) {
							
						}else{
							j++;
							if(data[i][j] == 0){
								data[i][j] = data[i][k];
								data[i][k] = 0;
							}
						}
					} else {
						data[i][j] = data[i][k];
						data[i][k] = 0;
					}
				}
			}
			break;
		case 1:
			for (i = 0; i < 4; i++) {
				j = 0;
				for (k = 1; k < 4; k++) {
					if (data[j][i] != 0) {
						if (data[j][i] == data[k][i]) {
							data[j][i] += data[k][i];
							data[k][i] = 0;
							j++;
						} else if (data[k][i]==0) {
							continue;
						}else{
							j++;
							if(data[j][i] == 0){
								data[j][i] = data[k][i];
								data[k][i] = 0;
							}
						}
					} else {
						data[j][i] = data[k][i];
						data[k][i] = 0;
					}
				}
			}
			break;
			
		case 2:
			for (i = 0; i < 4; i++) {
				j = 3;
				for (k = 2; k >= 0; k--) {
					if (data[i][j] != 0) {
						if (data[i][j] == data[i][k]) {
							data[i][j] += data[i][k];
							data[i][k] = 0;
							j--;
						} else if (data[i][k]==0) {
							
						}else{
							j--;
							if(data[i][j] == 0){
								data[i][j] = data[i][k];
								data[i][k] = 0;
							}
						}
					} else {
						data[i][j] = data[i][k];
						data[i][k] = 0;
					}
				}
			}
			break;
		case 3:
			for (i = 0; i < 4; i++) {
				j = 3;
				for (k = 2; k >= 0; k--) {
					if (data[j][i] != 0) {
						if (data[j][i] == data[k][i]) {
							data[j][i] += data[k][i];
							data[k][i] = 0;
							j--;
						} else if (data[k][i]==0) {
							continue;
						}else{
							j--;
							if(data[j][i] == 0){
								data[j][i] = data[k][i];
								data[k][i] = 0;
							}
						}
					} else {
						data[j][i] = data[k][i];
						data[k][i] = 0;
					}
				}
			}
			break;
	}
	
	for (i = 0; i < 4; i++) {
		for (j = 0; j < 4; j++) {
			printf("%d ", data[i][j]);
		}
		printf("\n");
	}
	
	return 0;
}
