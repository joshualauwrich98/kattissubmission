#include <stdio.h>

int main  () {
	int a,b,min,max,i;
	scanf("%d %d", &a, &b);
	min = a+1;
	max = b+1;
	if (min > max) {
		int temp = min;
		min = max;
		max = temp;
	}
	for (i = min ; i <= max; i++) {
		printf("%d\n", min++);
	}
	return 0;
}
