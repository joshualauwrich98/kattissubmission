#include <iostream>
#include <set>
#include <string>

using namespace std;

int main () {
	int n;
	set<string> st;
	scanf("%d", &n);
	string tim, name;
	pair< set<string>::iterator,bool> ptr;
	for (int i = 0; i < n; i++) {
		cin >> tim >> name;
		if (st.size() == 12) continue;
		ptr = st.insert(tim);
		
		if (ptr.second) {
			cout << tim << " " << name << endl;
		}
	}
	return 0;
}
