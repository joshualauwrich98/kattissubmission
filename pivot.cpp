#include <stdio.h>

int max(int num1, int num2) {
	return num1 > num2 ? num1 : num2;
}

int min (int num1, int num2) {
	return num1 < num2 ? num1 : num2;
}

int main () {
	int byk, i, j;
	scanf("%d", &byk);
	int data[byk], small[byk], large[byk];
	for (i = 0; i < byk; i++) {
		scanf("%d", &data[i]);
	}
	
	small[0] = data[0];
	large[byk-1] = data[byk-1];
	
	for (i = 1; i < byk; i++) {
		small[i] = max(data[i], small[i-1]);
	}
	
	for (i = byk-2; i >= 0; i--) {
		large[i] = min(data[i], large[i+1]);
	}
	
	int res = 0;
	
	for (i = 0; i < byk; i++) {
		if (small[i] <= data[i] && large[i] >= data[i]) {
			res++;
		}
	}
	
	printf("%d\n", res);
	
	return 0;
}
