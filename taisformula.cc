#include<stdio.h>

int main() {
    double v1, v2;
    int t1, t2,n, i;
    scanf("%d", &n);
    scanf("%d %lf", &t1, &v1);
    double area = 0;
    n--;
    while(n--) {
        scanf("%d %lf", &t2, &v2);
        area += (((v2 + v1) / 2) * (t2 - t1)) / 1000;
        t1 = t2;
        v1 = v2;
    }

    printf("%f\n", area);

    return 0;
}