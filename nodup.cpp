#include<iostream> 
#include<set>
#include<string>
#include<vector>

using namespace std;

vector<string> split(const string& str, const string& delim)
{
    vector<string> tokens;
    size_t prev = 0, pos = 0;
    do
    {
        pos = str.find(delim, prev);
        if (pos == string::npos) pos = str.length();
        string token = str.substr(prev, pos-prev);
        if (!token.empty()) tokens.push_back(token);
        prev = pos + delim.length();
    }
    while (pos < str.length() && prev < str.length());
    return tokens;
}

int main () {
	set<string> st;
	pair< set<string>::iterator,bool> ptr;
	string temp;
	bool status = false;
	getline(cin, temp);
	vector<string> results = split(temp, " ");
	for (int i = 0; i < results.size(); i++) {
		ptr = st.insert(results[i]);
		if (!ptr.second) {
			status = true;
			break;
		}
	}
	
	if (status) {
		printf("no\n");
	} else {
		printf("yes\n");
	}
	return 0;
	
}
