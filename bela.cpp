#include <stdio.h>

int main () {
	int n, i,j,res=0;
	char dominant;
	char temp[4];
	scanf("%d %c", &n, &dominant);
	for (i = 0; i < n; i++) {
		for (j = 0; j < 4; j++) {
			scanf("%s", temp);
			if (temp[0] == 'A') {
				res += 11;
			} else if (temp[0] == 'K') {
				res += 4;
			} else if (temp[0] == 'Q') {
				res += 3;
			} else if (temp[0] == 'J') {
				if (temp[1] == dominant) {
					res += 20;
				} else {
					res += 2;
				}
			} else if (temp[0] == 'T') {
				res += 10;
			} else if (temp[0] == '9') {
				if (temp[1] == dominant) {
					res += 14;
				}
			}
		}
	}
	
	printf("%d\n", res);
	return 0;
}
