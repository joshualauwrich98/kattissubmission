#include <stdio.h>
#include <string.h>

int main () {
	char input[301];
	scanf("%s", input);
	int count = 0, j = 0;
	char con[3] = {'P', 'E', 'R'};
	for (int i = 0; i < strlen(input); i++) {
		if (input[i] != con[j++]) {
			count++;
		}
		if (j > 2) j = 0;
	}
	
	printf("%d\n", count);
	return 0;
}
