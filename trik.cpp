#include <stdio.h>
#include <string.h>

int main () {
	char q[51];
	scanf("%s", q);
	int currBall = 1;
	for (int i = 0; i < strlen(q); i++) {
		if (q[i] == 'A') {
			if (currBall == 1) currBall = 2;
			else if (currBall == 2) currBall = 1;
		} else if (q[i] == 'B') {
			if (currBall == 2) currBall = 3;
			else if (currBall == 3) currBall = 2;
		} else {
			if (currBall == 1) currBall = 3;
			else if (currBall == 3) currBall = 1;
		}
	}
	
	printf("%d\n", currBall);
	return 0;
}
