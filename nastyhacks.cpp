#include <stdio.h>

int main () {
	int kasus, advertise, nadvertise, cost;
	scanf("%d", &kasus);
	while (kasus--) {
		scanf("%d %d %d", &nadvertise, &advertise, &cost);
		advertise -= cost;
		if (advertise > nadvertise) {
			printf("advertise\n");
		} else if (advertise < nadvertise) {
			printf("do not advertise\n");
		} else {
			printf("does not matter\n");
		}
	}
	return 0;
}
