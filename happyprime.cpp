#include <stdio.h>

bool isPrime(int n) 
{ 
    // Corner cases 
    if (n <= 1)  return false; 
    if (n <= 3)  return true; 
  
    // This is checked so that we can skip  
    // middle five numbers in below loop 
    if (n%2 == 0 || n%3 == 0) return false; 
  
    for (int i=5; i*i<=n; i=i+6) 
        if (n%i == 0 || n%(i+2) == 0) 
           return false; 
  
    return true; 
}

bool isHappy (int n) {
	if (n == 1) {
		return true;
	} else {
		int num = n;
		int res = 0;
		int temp;
		while (num > 0) {
			temp = num%10;
			res += (temp*temp);
			num/=10;
		}
		return isHappy(res);
	}
}

int main () {
	int kasus, k, num, i;
	scanf("%d", &kasus);
	while (kasus--) {
		scanf("%d %d", &k, &num);
		if (isPrime(num) && isHappy(num)) {
			printf("%d %d YES\n", k, num);
		} else {
			printf("%d %d NO\n", k, num);
		}
	}
	return 0;
}
