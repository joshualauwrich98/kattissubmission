#include <stdio.h>

int main () {
	int x, i, n, p, res;
	scanf("%d", &x);
	scanf("%d", &n);
	res = x*(n+1);
	for (i = 0; i < n; i++) {
		scanf("%d", &p);
		res -= p;
	}
	printf("%d\n",res);
	return 0;
}
