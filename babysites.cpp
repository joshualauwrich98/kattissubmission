#include <stdio.h>

int main () {
	int a,i,j,n,prev=0,count=0;
	scanf("%d", &n);
	bool res = true;
	char mumble[7];
	for (i = 0; i < n; i++) {
		if (scanf("%d", &a) == 1) {
			if (prev+count+1 != a) {
				res = false;
			} else {
				prev = a;
			}
			count = 0;
		} else {
			scanf("%s", mumble);
			count++;
		}
	}
	if (res) {
		printf("makes sense\n");
	} else {
		printf("something is fishy\n");
	}
	return 0;
}
