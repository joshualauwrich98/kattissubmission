#include <iostream>
#include <string>

using namespace std;

int getValue (string kata, int start, int end) {
	int res = 0;
	for (int i = start; i<=end; i++) {
		res += kata[i]-'A';
	}
	return res;
}

string rotate (string kata, int start, int end) {
	int val = getValue(kata, start, end);
	string hasil = "";
	for (int i = start; i <= end; i++) {
		int temp = kata[i] - 'A';
		temp += val;
		temp %= 26;
		char c = temp + 'A';
		hasil += c;
	}
	
	return hasil;
}

int main () {
	int i,j,length,mid;
	string kata;
	getline(cin, kata);
	length = kata.length();
	mid = (length/2)-1;
	string sub1 = rotate(kata,0,mid);
	string sub2 = rotate(kata,mid+1,length-1);
	string hasil = "";
	for (int i = 0; i <= mid; i++) {
		int temp = sub1[i] - 'A';
		temp += sub2[i] - 'A';
		temp %= 26;
		char c = temp + 'A';
		hasil += c;
	}
	
	cout << hasil << endl;
	return 0;
}
