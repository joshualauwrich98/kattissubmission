#include <stdio.h>

int main () {
	double cost, p, l;
	int n;
	scanf("%lf", &cost);
	scanf("%d", &n);
	double res = 0;
	for (int i = 0; i < n; i++) {
		scanf("%lf %lf", &p, &l);
		res += (p*l);
	}
	res *= cost;
	printf("%.7lf\n", res);
	return 0;
}
