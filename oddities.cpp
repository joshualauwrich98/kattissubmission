#include <stdio.h>

int main () {
	int kasus, temp;
	scanf("%d", &kasus);
	while (kasus--) {
		scanf("%d", &temp);
		if (temp%2 == 0) {
			printf("%d is even\n", temp);
		} else {
			printf("%d is odd\n", temp);
		}
	}
	return 0;
}
