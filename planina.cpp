#include <stdio.h>
#include <math.h>

int getPerpangkatan(int num) {
	int res = 0;
	for (int i = 0; i < num; i++) {
		res += pow(2,i);
	}
	return res;
}

int main () {
	int input;
	scanf("%d", &input);
	long long int res;
	res = 2 + getPerpangkatan(input);
	res = pow(res, 2);
	printf("%lld\n", res);
	return 0;
}
