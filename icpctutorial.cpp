#include <stdio.h>
#include <math.h>

int main(){
	long long m, n, t, temp = 0;
	bool ok = true;
	int i;
	scanf ("%lld %lld %lld", &m, &n, &t);
	if (t == 1){
		if (n > 12){
			ok = false;
			printf("TLE\n");
		}
		else{
			temp = 1;
			for (i = n; i > 0; i--){
				temp *= i;
				if (temp > m){
					printf("TLE\n");
					ok = false;
					break;
				}
			}
		}
	}
	if (t==2){
		if (n > 29){
			ok = false;
			printf("TLE\n");
		}
		else{
			temp = 1;
			for (i = 1; i <= n; i++){
				temp *= 2;
				if (temp > m){
					printf("TLE\n");
					ok = false;
					break;
				}
			}
		}
	}
	if (t==3){
		if (n > 177){
			ok = false;
			printf("TLE\n");
		}
		else{
			temp = 1;
			for (i = 1; i <= 4; i++){
				temp *= n;
				if (temp > m){
					printf("TLE\n");
					ok = false;
					break;
				}
			}
		}
	}
	if (t==4){
		if (n > 1000){
			ok = false;
			printf("TLE\n");
		}
		else{
			temp = 1;
			for (i = 1; i <= 3; i++){
				temp *= n;
				if (temp > m){
					printf("TLE\n");
					ok = false;
					break;
				}
			}
		}
	}
	if (t==5){
		if (n > 31684){
			ok = false;
			printf("TLE\n");
		}
		else{
			temp = 1;
			for (i = 1; i <= 2; i++){
				temp *= n;
				if (temp > m){
					printf("TLE\n");
					ok = false;
					break;
				}
			}
		}
	}
	if (t==6){
		if (n > 100000000){
			ok = false;
			printf("TLE\n");
		}
		else{
			if (n * log2(n) > m){
				ok = false;
				printf("TLE\n");
			}
		}
	}
	if (t==7){
		if (n > m){
			ok = false;
			printf ("TLE\n");
		}
	}
	if (ok) printf ("AC\n");
	return 0;
}
