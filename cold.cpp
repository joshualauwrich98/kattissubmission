#include <stdio.h>

int main () {
	int i, sum, temp, jumlah;
	scanf("%d", &jumlah);
	sum = 0;
	for (i = 0; i < jumlah; i++) {
		scanf("%d", &temp);
		if (temp < 0) sum++;
	}
	
	printf("%d\n", sum);
	return 0;
}
