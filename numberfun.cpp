#include <stdio.h>

int main () {
	bool status;
	int kasus;
	double num1, num2, num3;
	scanf("%d", &kasus);
	while (kasus--) {
		scanf("%lf %lf %lf", &num1, &num2, &num3);
		status = false;
		if (num1 + num2 == num3) {
			status = true;
		} else if (num1 - num2 == num3 || num2 - num1 == num3) {
			status = true;
		} else if (num1*num2 == num3) {
			status = true;
		} else if (num1/num2 == num3 || num2/num1 == num3) {
			status = true;
		}
				
		if (status) {
			printf("Possible\n");
		} else {
			printf("Impossible\n");
		}
	}
	return 0;
}
