#include <stdio.h>

int main () {
	int n, res,i,tempA,tempB,before;
	while (true) {
		scanf("%d", &n);
		if (n == -1) break;
		before = 0;
		res = 0;
		for (i = 0; i < n; i++) {
			scanf("%d %d", &tempA, &tempB);
			before = tempB-before;
			res += (before*tempA);
			before = tempB;
		}
		
		printf("%d miles\n", res);
	}
	return 0;
}
