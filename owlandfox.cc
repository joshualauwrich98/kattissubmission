#include<stdio.h>

int main() {
    int kasus, input, i, temp, temp1;
    scanf("%d", &kasus);
    while(kasus--) {
        scanf("%d", &input);
        temp = 0;
        while(input > 0) {
            temp1 = input%10;
            if (temp1 != 0) break;
            else {
                temp++;
                input/=10;
            }
        }

        int res = input-1;
        for (i = 1; i <= temp; i++) {
            res *= 10;
        }

        printf("%d\n", res);
    }

    return 0;
}