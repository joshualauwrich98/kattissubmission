#include<iostream>
#include<string>

using namespace std;

int main() {
    string temp;
    int kasus, n, i;
    long long int total, input;
    cin >> kasus;
    getline(cin, temp);

    while(kasus--) {
        getline(cin,temp);
        cin >> n;
        total = 0;
        for(i = 0; i < n; i++) {
            cin >> input;
            total+=input;
            if (total > n) total %= n;
        }

        if (total == 0 || total == n) cout << "YES\n";
        else cout << "NO\n";
    }
    return 0;
}